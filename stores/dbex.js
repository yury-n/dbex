'use strict';

const alt = require('lib/da/dbex/alt-inst');

const DBEXActions = require('lib/da/dbex/actions/dbex');

class DBEXStore {

    constructor() {
        this.bindActions(DBEXActions);

        this.tablesByServers = [];
        this.tableTreeExpanded = false;
    }

    onFetchTableListingDone(tablesByServers) {
        this.tablesByServers = tablesByServers;
    }

    onExpandTableTree() {
        this.tableTreeExpanded = true;
    }

    onCollapseTableTree() {
        this.tableTreeExpanded = false;
    }

}

module.exports = alt.createStore(DBEXStore);