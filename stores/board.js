'use strict';

const alt = require('lib/da/dbex/alt-inst');

const DBEXActions = require('lib/da/dbex/actions/dbex');

const TableAutosuggestStore = require('lib/da/dbex/stores/table-autosuggest');
const ColumnAutosuggestStore = require('lib/da/dbex/stores/column-autosuggest');

class BoardStore {

    constructor() {
        this.bindActions(DBEXActions);

        //this.colorPalette = ['#FAEDD6', '#E9F3DF'];
        //this.colorCursor = 0;

        this.boardItemIdSequence = 0;

        this.items = [this.getEmptyBoardItemStructure(this.boardItemIdSequence)];
        this._setActiveBoardItem(this.boardItemIdSequence);
    }

    onSpawnNewBoardItem(inputValue) {
        this._spawnNewBoardItem(inputValue);
    }

    _spawnNewBoardItem(inputValue) {
        let nextBoardItemId = ++this.boardItemIdSequence;
        this.items.push(this.getEmptyBoardItemStructure(nextBoardItemId));
        this._setActiveBoardItem(nextBoardItemId);
        if (inputValue) {
            let boardItem = this.getBoardItemById(nextBoardItemId);
            boardItem.inputValue = inputValue;
        }
    }

    onRemoveBoardItem(boardItemId) {
        let boardItemIndex = this.items.findIndex(item => item.id == boardItemId)
        this.items.splice(boardItemIndex, 1);
    }

    onClearBoard() {
        this.items = [];
        this.boardItemIdSequence = 0;
        this._spawnNewBoardItem();
    }

    getEmptyBoardItemStructure(boardItemId) {
        return {
            'id': boardItemId,
            'inputValue': '',
            'inputCommitted': false,
            'hasAutosuggestions': false,
            'active': false,
            'serverName': '',
            'tableName': '',
            'hasDotSeparator': false,
            'columnName': '',
            'hasEqualsSign': false,
            'columnValue': '',
            'loading': false,
            'results': {
                'table': {
                    'columnNames': [],
                    'rows': []
                }
            }
        };
    }

    _setActiveBoardItem(boardItemId) {

        this.items.forEach(item => {
            item.active = false;
        });

        let boardItem = this.getBoardItemById(boardItemId);

        boardItem.active = true;
    }

    getBoardItemById(boardItemId) {
        return this.items.find(item => item.id == boardItemId);
    }

    onSetActiveBoardItem(boardItemId) {
        this._setActiveBoardItem(boardItemId);
    }

    onChangeInput({boardItemId, inputTablePart, hasDotSeparator, inputColumnPart, hasEqualsSign, inputColumnValuePart}) {

        this.waitFor([TableAutosuggestStore.dispatchToken, ColumnAutosuggestStore.dispatchToken]);

        let boardItem = this.getBoardItemById(boardItemId);

        boardItem['tableName'] = inputTablePart || '';
        boardItem['hasDotSeparator'] = hasDotSeparator;
        boardItem['columnName'] = inputColumnPart || '';
        boardItem['hasEqualsSign'] = hasEqualsSign;
        boardItem['columnValue'] = (inputColumnValuePart || '').trim(); // trim is usefull when a value is copied from a TD extra spaces
        boardItem['inputCommitted'] = false;
        boardItem['hasAutosuggestions'] = (TableAutosuggestStore.getState().active || ColumnAutosuggestStore.getState().active);

        this._buildInputValue(boardItemId);
    }

    onInsertDotSeparator({boardItemId, serverName}) {

        let boardItem = this.getBoardItemById(boardItemId);

        boardItem['serverName'] = serverName;
        boardItem['hasDotSeparator'] = true;

        this._buildInputValue(boardItemId);
    }

    onCommitInput({boardItemId}) {

        let boardItem = this.getBoardItemById(boardItemId);

        boardItem['loading'] = true;
        boardItem['active'] = false;

        this._buildInputValue(boardItemId);
    }

    onSelectSuggestedTable({boardItemId, suggestedTableWithServer: {serverName, tableName}, selectedBy}) {

        let boardItem = this.getBoardItemById(boardItemId);

        boardItem['serverName'] = serverName;
        boardItem['tableName'] = tableName;
        boardItem['inputValue'] = tableName;
        boardItem['inputCommitted'] = true;
        boardItem['loading'] = true;

        if (selectedBy == 'enter') {
            boardItem['active'] = false;
        }

        this._buildInputValue(boardItemId);
    }

    onSelectSuggestedColumn({boardItemId, selectedColumn}) {

        let boardItem = this.getBoardItemById(boardItemId);

        boardItem['columnName'] = selectedColumn;

        this._buildInputValue(boardItemId);
    }

    onFetchTableDone({boardItemId, result: [columnNames, rows]}) {
        let boardItem = this.getBoardItemById(boardItemId);

        boardItem['results']['table'] = {columnNames, rows};
        boardItem['loading'] = false;
        if (!rows.length) {
            boardItem['active'] = true;
        }
    }

    onFetchTableColumnsDone({boardItemId}) {
        boardItem['loading'] = false;
    }

    onSelectTableFromTableTree({serverName, tableName}) {

        this.boardItemIdSequence = 0;
        let boardItemId = this.boardItemIdSequence;

        this.items = [this.getEmptyBoardItemStructure(boardItemId)];
        let boardItem = this.getBoardItemById(boardItemId);
        boardItem['serverName'] = serverName;
        boardItem['tableName'] = tableName;

        this._buildInputValue(boardItemId);
    }

    _buildInputValue(boardItemId) {

        let boardItem = this.getBoardItemById(boardItemId);

        boardItem['inputValue'] = boardItem['tableName'] + (boardItem['hasDotSeparator'] ? '.' : '') +
                                  boardItem['columnName'] + (boardItem['hasEqualsSign'] ? '=' : '') +
                                  boardItem['columnValue'];
    }

}

module.exports = alt.createStore(BoardStore);