'use strict';

const alt = require('lib/da/dbex/alt-inst');

const DBEXActions = require('lib/da/dbex/actions/dbex');

class TableTreeStore {

    constructor() {
        this.bindActions(DBEXActions);

        let tableTreeNodes = [];
    }

    onFetchTableListingDone(tableNamesByServers) {

        this.tableTreeNodes = [];

        for (var serverName in tableNamesByServers) {

            let tablesNames = tableNamesByServers[serverName];

            let serverNode = {'text': serverName, nodes: []};

            tablesNames.forEach(tableName => {
                let tableNameParts = tableName.split('_', 2);
                if (tableNameParts.length == 0) {
                    serverNode.nodes.push({'text': tableName});
                } else {
                    let serverSubNode = serverNode.nodes.find(node => node.text == tableNameParts[0]);
                    if (serverSubNode) {
                        serverSubNode.nodes.push({'text': tableName, 'server': serverName});
                    } else {
                        serverNode.nodes.push({'text': tableNameParts[0], 'server': serverName,  'nodes': [{'text': tableName, 'server': serverName}]});
                    }
                }
            });

            serverNode.nodes.forEach((node, index) => {
                if (node.nodes && node.nodes.length == 1) {
                    serverNode.nodes[index] = {'text': node.nodes[0].text, 'server': node.nodes[0].server};
                }
            });

            this.tableTreeNodes.push(serverNode);
        }
    }

}

module.exports = alt.createStore(TableTreeStore);