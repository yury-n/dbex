'use strict';

const alt = require('lib/da/dbex/alt-inst');

const DBEXActions = require('lib/da/dbex/actions/dbex');
const ColumnAutosuggestActions = require('lib/da/dbex/actions/column-autosuggest');

// TODO: think if it's sensible to extract similar logic from table/column-autosuggest to a base class

class ColumnAutosuggestStore {

    constructor() {
        this.bindActions(DBEXActions);
        this.bindActions(ColumnAutosuggestActions);

        // suggestions source
        this.tableColumns = {};

        this.serverName = null;
        this.tableName = null;
        this.inputValue = '';

        this.componentPosition = {};
        this.suggestedColumnsLimit = 5;

        this.setCleanSuggestionState();
    }

    setCleanSuggestionState() {

        this.suggestedColumns = [];

        this.hasFullMatch = false;

        this.active = false;
        this.selectedIndex = 0;
    }

    onExpandTableTree() {
        this.setCleanSuggestionState();
    }

    onFetchTableDone({serverName, tableName, result: [columnNames]}) {
        if (!this.tableColumns[serverName]) {
            this.tableColumns[serverName] = {};
        }
        this.tableColumns[serverName][tableName] = columnNames;
    }

    onInsertDotSeparator({serverName, tableName}) {
        this.serverName = serverName;
        this.tableName = tableName;
    }

    onFetchTableColumnsDone({serverName, tableName, columnNames}) {
        if (!this.tableColumns[serverName]) {
            this.tableColumns[serverName] = {};
        }
        this.tableColumns[serverName][tableName] = columnNames;
    }

    onChangeInput({serverName, tableName, inputColumnPart, inputColumnBoundingClientRect}) {

        if (!inputColumnPart || inputColumnPart == this.inputValue) {
            return;
        }

        this.inputValue = inputColumnPart;
        this.serverName = serverName;
        this.tableName = tableName;

        this.componentPosition = {
            left: document.body.scrollLeft + inputColumnBoundingClientRect.left,
            top: document.body.scrollTop + inputColumnBoundingClientRect.top + inputColumnBoundingClientRect.height
        };

        this.setCleanSuggestionState();

        if (!this.inputValue) {
            return;
        }

        this.buildSuggestions();

        this.active = (this.suggestedColumns.length && !this.hasFullMatch);
    }

    onSelectSuggestedColumn({selectedColumn}) {
        this.inputValue = selectedColumn;
        this.setCleanSuggestionState();
    }

    onClearSuggestions() {
        this.setCleanSuggestionState();
    }

    onUpdateSelectedIndex(newSelectedIndex) {
        if (newSelectedIndex < 0) {
            newSelectedIndex = this.suggestedColumns.length - 1;
        } else if (newSelectedIndex > this.suggestedColumns.length - 1) {
            newSelectedIndex = 0;
        }
        this.selectedIndex = newSelectedIndex;
    }

    buildSuggestions() {
        let matchedColumns = this.matchSubstring();
        this.suggestedColumns = matchedColumns.slice(0, this.suggestedColumnsLimit);
    }

    matchSubstring() {

        let suggestedColumns = [];

        if (this.tableColumns[this.serverName] && this.tableColumns[this.serverName][this.tableName]) {

            this.tableColumns[this.serverName][this.tableName].forEach(columnName => {
                if (columnName.includes(this.inputValue)) {
                    suggestedColumns.push(columnName);
                    if (columnName == this.inputValue) {
                        this.hasFullMatch = true;
                    }
                }
            });

        }

        return suggestedColumns;
    }

}

module.exports = alt.createStore(ColumnAutosuggestStore);