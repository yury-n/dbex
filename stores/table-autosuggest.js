'use strict';

const alt = require('lib/da/dbex/alt-inst');

const DBEXActions = require('lib/da/dbex/actions/dbex');
const TableAutosuggestActions = require('lib/da/dbex/actions/table-autosuggest');

const Fuse = require('lib/tp/fuse');
// fuse instance
let fuse;

class TableAutosuggestStore {

    constructor() {
        this.bindActions(DBEXActions);
        this.bindActions(TableAutosuggestActions);

        // suggestions source
        this.tablesWithServers = [];

        this.inputValue = '';

        this.componentPosition = {};
        this.suggestedTablesLimit = 5;

        this.setCleanSuggestionState();
    }

    setCleanSuggestionState() {

        this.suggestedTablesWithServers = [];

        this.hasFullMatch = false;

        this.active = false;
        this.selectedIndex = 0;
    }

    onExpandTableTree() {
        this.setCleanSuggestionState();
    }

    onFetchTableListingDone(tablesByServers) {

        this.tablesWithServers = [];

        for (var serverName in tablesByServers) {

            let tables = tablesByServers[serverName];

            tables.forEach(tableName => {
                this.tablesWithServers.push({serverName, tableName});
            });
        }

        fuse = new Fuse(this.tablesWithServers, {'keys': ['tableName'], 'threshold': '0.4'});

    }

    onChangeInput({inputTablePart, inputBoundingClientRect}) {

        if (!inputTablePart || inputTablePart == this.inputValue) {
            return;
        }

        this.inputValue = inputTablePart;

        this.componentPosition = {
            left: document.body.scrollLeft + inputBoundingClientRect.left,
            top: document.body.scrollTop + inputBoundingClientRect.top + inputBoundingClientRect.height
        };

        this.setCleanSuggestionState();

        if (!this.inputValue) {
            return;
        }

        this.buildSuggestions();

        this.active = (this.suggestedTablesWithServers.length && !this.hasFullMatch);
    }

    onSelectSuggestedTable({suggestedTableWithServer: {tableName}}) {
        this.inputValue = tableName;
        this.setCleanSuggestionState();
    }

    onClearSuggestions() {
        this.setCleanSuggestionState();
    }

    onUpdateSelectedIndex(newSelectedIndex) {
        if (newSelectedIndex < 0) {
            newSelectedIndex = this.suggestedTablesWithServers.length - 1;
        } else if (newSelectedIndex > this.suggestedTablesWithServers.length - 1) {
            newSelectedIndex = 0;
        }
        this.selectedIndex = newSelectedIndex;
    }

    buildSuggestions() {
        let matchedTables = this.matchSubstring();
        // appropriate only for the result of substring matching
        this.sortMatchedTables(matchedTables);
        if (matchedTables.length < this.suggestedTablesLimit) {
            // TODO: fuzzy search is disabled for now. need to make it async
            //matchedTables.push(...this.matchFuzzy());
        }
        this.suggestedTablesWithServers = matchedTables.slice(0, this.suggestedTablesLimit);
    }

    matchSubstring() {

        let matchedTables = [];

        this.tablesWithServers.forEach(table => {
            let {tableName} = table;
            if (tableName.includes(this.inputValue)) {
                matchedTables.push(table);
                if (tableName == this.inputValue) {
                    this.hasFullMatch = true;
                }
            }
        });

        return matchedTables;
    }

    matchFuzzy() {
        if (!fuse) {
            return [];
        }
        // TODO: not great performance on long strings matched against our long list of tables
        // need to throttle
        return fuse.search(this.inputValue);
    }

    sortMatchedTables(matchedTables) {

        if (!matchedTables.length) {
            return;
        }

        matchedTables.sort((a, b) => {
            if (a.tableName.indexOf(this.inputValue) != b.tableName.indexOf(this.inputValue)) {
                return a.tableName.indexOf(this.inputValue) < b.tableName.indexOf(this.inputValue) ? -1 : 1;
            } else {
                if (a.tableName.length != b.tableName.length) {
                    return a.tableName.length < b.tableName.length ? -1 : 1;
                } else {
                    if (a.tableName == b.tableName) {
                        // datamine tables get lower weight
                        if (a.serverName == 'datamine' && b.serverName != 'datamine') {
                            return 1;
                        } else if (b.serverName == 'datamine' && a.serverName != 'datamine') {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                    return a.tableName < b.tableName ? -1 : 1;
                }
            }
        });
    }


}

module.exports = alt.createStore(TableAutosuggestStore);