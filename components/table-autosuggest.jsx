'use strict';

const {React, ReactBase} = require('lib/tp/react-base');

const TableAutosuggestActions = require('lib/da/dbex/actions/table-autosuggest');

const Autosuggest = require('lib/da/dbex/components/autosuggest');

class TableAutosuggest extends ReactBase {

    handleBlur() {
        TableAutosuggestActions.clearSuggestions();
    }

    handleUpdateSelectedIndex(index) {
        TableAutosuggestActions.updateSelectedIndex(index);
    }

    handleSelectSuggestedTable({selectedIndex, selectedBy}) {
        let {suggestedTablesWithServers} = this.props;
        let suggestedTableWithServer = suggestedTablesWithServers[selectedIndex];
        this.props.handleSelectSuggestedTable({suggestedTableWithServer, selectedBy});
    }

    render() {

        let {active, inputValue} = this.props;
        let {suggestedTablesWithServers, ...propsToPass} = this.props;

        let tableSuggestions = [];

        if (active) {

            suggestedTablesWithServers.forEach((suggestedTableWithServer, index) => {

                let {tableName, serverName} = suggestedTableWithServer;

                tableSuggestions.push(<TableSuggestion key={index}
                                                       index={index}
                                                       tableName={tableName}
                                                       serverName={serverName}
                                                       inputValue={inputValue} />);
            });
        }

        return (
            <Autosuggest className="table-autosuggest"
                         {...propsToPass}
                         suggestions={tableSuggestions}
                         handleBlur={this.handleBlur}
                         handleUpdateSelectedIndex={this.handleUpdateSelectedIndex}
                         handleSelectSuggestion={this.handleSelectSuggestedTable}   />
        )
    }
}

class TableSuggestion extends ReactBase {

    render() {

        let {inputValue, tableName, serverName} = this.props;

        // this is to highlight the matched part
        let suggestedTableName = null;
        if (tableName.indexOf(inputValue) > -1) {
            let tableNameParts = tableName.split(inputValue, 2);
            let beforeMatch = tableNameParts[0];
            let afterMatch = tableNameParts[1];
            suggestedTableName = <span>{beforeMatch}<strong>{inputValue}</strong>{afterMatch}</span>;
        } else {
            suggestedTableName = <span>{tableName}</span>;
        }

        return (
            <span>
                {suggestedTableName}
                <span className="server-name">{serverName}</span>
            </span>
        )
    }
}

module.exports = TableAutosuggest;