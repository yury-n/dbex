'use strict';

const {React, ReactBase} = require('lib/tp/react-base');

class ResultsTable extends ReactBase {

    render() {

        let {columnNames, rows} = this.props;

        let tableTHs = [];
        columnNames.forEach(fieldName => {
            tableTHs.push(<th key={fieldName}>{fieldName}</th>);
        });

        let tableBodyTRs = [];
        rows.forEach((row, rowIndex) => {
            let TDs = [];
            columnNames.forEach(fieldName => {
                let value = row[fieldName];
                TDs.push(<td key={fieldName}>{value}</td>);
            });
            tableBodyTRs.push(<tr key={rowIndex}>{TDs}</tr>);
        });

        return (
            <table className="table table-striped table-bordered">
                <thead>
                    <tr>{tableTHs}</tr>
                </thead>
                <tbody>
                    {tableBodyTRs}
                </tbody>

            </table>
        )
    }
}

module.exports = ResultsTable;