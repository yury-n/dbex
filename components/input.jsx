'use strict';

const {React, ReactBase} = require('lib/tp/react-base');

class Input extends ReactBase {

    componentDidMount() {
        this._updateInputActiveness();
    }

    componentDidUpdate() {
        this._updateInputActiveness();
    }

    _updateInputActiveness() {
        let node = this.refs.input.getDOMNode();
        if (this.props.active) {
            node.focus();
        } else {
            node.blur();
        }
    }

    handleChange(event) {
        let inputValue = event.target.value;
        let inputBoundingClientRect = event.target.getBoundingClientRect();
        this.props.handleChange({inputValue, inputBoundingClientRect});
    }

    handleKeyDown(event) {
        if (event.keyCode == '13') { // enter
            if (!this.props.committed && this.props.committableOnEnter) {
                this.props.handleCommit();
            }
        }
        if (event.keyCode == '9') { // tab
            event.preventDefault();
        }
    }

    render() {

        return (
            <input type="text"
                   className="form-control input input-xlg"
                   ref="input"
                   value={this.props.value}
                   onFocus={this.props.handleFocus}
                   onKeyDown={this.handleKeyDown}
                   onChange={this.handleChange} />
        )
    }
}

module.exports = Input;