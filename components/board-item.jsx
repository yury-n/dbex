'use strict';

const {React, ReactBase} = require('lib/tp/react-base');

const DBEXActions = require('lib/da/dbex/actions/dbex');

const Input = require('lib/da/dbex/components/input');
const CloseIcon = require('lib/da/dbex/components/close-icon');
const Results = require('lib/da/dbex/components/results');

class BoardItem extends ReactBase {

    handleFocus() {

        let {id: boardItemId, active} = this.props;

        if (!active) {
            DBEXActions.setActiveBoardItem(boardItemId);
        }
    }

    handleChangeInput({inputValue, inputBoundingClientRect}) {

        let {id: boardItemId, inputValue: prevInputValue, serverName, tableName, columnName} = this.props;

        if (inputValue.slice(-1) == '.' && !prevInputValue.includes('.')) {
            // inserted dot is kinda a big deal
            this.props.handleInsertDotSeparator({boardItemId, tableName});
            return;
        }

        let hasDotSeparator = inputValue.includes('.');
        let hasEqualsSign = inputValue.includes('=');

        let [inputTableAndColumnPart, inputColumnValuePart] = inputValue.split('=');
        let [inputTablePart, inputColumnPart] = inputTableAndColumnPart.split('.');

        let data = {
            boardItemId,
            inputValue,
            inputTablePart,
            hasDotSeparator,
            inputColumnPart,
            hasEqualsSign,
            inputColumnValuePart,
            inputBoundingClientRect
        };

        // if column has changed,
        // a bit of extra info is required about the event
        if (inputColumnPart && inputColumnPart != columnName) {
            let {top, bottom, left, right, width, height} = inputBoundingClientRect;
            let inputColumnBoundingClientRect = {top, bottom, left, right, width, height};
            let columnPartOffset = this.refs.mirroredPreColumnPart.getDOMNode().offsetWidth;
            inputColumnBoundingClientRect['left'] += columnPartOffset;
            data['inputColumnBoundingClientRect'] = inputColumnBoundingClientRect;
            data['serverName'] = serverName;
            data['tableName'] = tableName;
        }

        DBEXActions.changeInput(data);
    }

    handleCommitInput() {

        let {id: boardItemId, serverName, tableName, columnName, columnValue} = this.props;

        this.props.handleCommitInput({boardItemId, serverName, tableName, columnName, columnValue});
    }

    handleCloseClick() {

        let {id: boardItemId} = this.props;

        DBEXActions.removeBoardItem(boardItemId);
    }

    render() {

        let {active, inputValue, inputCommitted, hasAutosuggestions, tableName} = this.props;
        let {results} = this.props;

        // hitting enter shouldn't commit when autosuggest is shown
        let inputCommittableOnEnter = !hasAutosuggestions;

        let closeIconHTML;

        if (inputValue) {

            closeIconHTML = <CloseIcon handleClick={this.handleCloseClick} />;
        }

        let preColumnPart = tableName + '.';

        return (
            <div className="board-item">

                <Input active={active}
                       value={inputValue}
                       committed={inputCommitted}
                       committableOnEnter={inputCommittableOnEnter}
                       handleFocus={this.handleFocus}
                       handleChange={this.handleChangeInput}
                       handleCommit={this.handleCommitInput} />

                {closeIconHTML}

                <div ref="mirroredPreColumnPart" className="form-control input input-xlg input-mirror">{preColumnPart}</div>

                <Results results={results} />

            </div>
        )
    }
}

module.exports = BoardItem;