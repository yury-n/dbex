'use strict';

const {React, ReactBase} = require('lib/tp/react-base');

const ColumnAutosuggestActions = require('lib/da/dbex/actions/column-autosuggest');

const Autosuggest = require('lib/da/dbex/components/autosuggest');

class ColumnAutosuggest extends ReactBase {

    handleBlur() {
        ColumnAutosuggestActions.clearSuggestions();
    }

    handleUpdateSelectedIndex(index) {
        ColumnAutosuggestActions.updateSelectedIndex(index);
    }

    handleSelectSuggestedColumn({selectedIndex}) {
        let {suggestedColumns} = this.props;
        let suggestedColumn = suggestedColumns[selectedIndex];
        this.props.handleSelectSuggestedColumn(suggestedColumn);
    }

    render() {

        let {active, inputValue} = this.props;
        let {suggestedColumns, ...propsToPass} = this.props;

        let columnSuggestions = [];

        if (active) {

            suggestedColumns.forEach((columnName, index) => {

                columnSuggestions.push(<ColumnSuggestion key={index}
                                                         index={index}
                                                         columnName={columnName}
                                                         inputValue={inputValue} />);
            });
        }

        return (
            <Autosuggest className="column-autosuggest"
                         {...propsToPass}
                         suggestions={columnSuggestions}
                         handleBlur={this.handleBlur}
                         handleUpdateSelectedIndex={this.handleUpdateSelectedIndex}
                         handleSelectSuggestion={this.handleSelectSuggestedColumn} />
        )
    }
}

class ColumnSuggestion extends ReactBase {

    render() {

        let {inputValue, columnName} = this.props;

        // this is to highlight the matched part
        let suggestedColumnName = null;
        if (columnName.indexOf(inputValue) > -1) {
            let columnNameParts = columnName.split(inputValue, 2);
            let beforeMatch = columnNameParts[0];
            let afterMatch = columnNameParts[1];
            suggestedColumnName = <span>{beforeMatch}<strong>{inputValue}</strong>{afterMatch}</span>;
        } else {
            suggestedColumnName = <span>{columnName}</span>;
        }

        return (
            <span>
                {suggestedColumnName}
            </span>
        )
    }
}

module.exports = ColumnAutosuggest;