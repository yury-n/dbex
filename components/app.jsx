'use strict';

const {React, ReactBase} = require('lib/tp/react-base');
const AltContainer = require('lib/tp/alt/AltContainer');

const DBEX = require('lib/da/dbex/components/dbex');
const DBEXStore = require('lib/da/dbex/stores/dbex');

class App extends ReactBase {

    render() {
        return (
            <div className="app">

                <AltContainer store={DBEXStore}>
                    <DBEX />
                </AltContainer>

            </div>
        )
    }
}

module.exports = App;