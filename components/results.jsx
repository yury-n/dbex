'use strict';

const {React, ReactBase} = require('lib/tp/react-base');

const ResultsTable = require('lib/da/dbex/components/results-table');

class Results extends ReactBase {

    render() {
        let {results} = this.props;

        let resultsTable = null;
        if (results.table.columnNames.length) {
            resultsTable = <ResultsTable {...results.table} />;
        }

        return (
            <div className="results">
                {resultsTable}
            </div>
        )
    }
}

module.exports = Results;