'use strict';

const {React, ReactBase} = require('lib/tp/react-base');

const DBEXActions = require('lib/da/dbex/actions/dbex');

const TreeView = require('lib/da/components/util/react-bootstrap-treeview');

class TableTree extends ReactBase {

    shouldComponentUpdate(nextProps) {
        return (nextProps.tableTreeNodes && !this.props.tableTreeNodes);
    }

    handleMouseEnter() {
        DBEXActions.expandTableTree();
    }

    handleMouseLeave() {
        DBEXActions.collapseTableTree();

    }

    handleTableSelect({serverName, tableName}) {
        DBEXActions.selectTableFromTableTree({serverName, tableName});
    }

    render() {

        let tableTreeHTML = null;
        if (this.props.tableTreeNodes) {

            let data = this.props.tableTreeNodes;

            tableTreeHTML = <TreeView handleSelect={this.handleTableSelect} data={data} levels={1} />;
        }

        return (
            <div className="table-tree" onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave}>
                <span className="glyphicon glyphicon-menu-hamburger"></span>
                {tableTreeHTML}
            </div>
        )
    }
}

module.exports = TableTree;