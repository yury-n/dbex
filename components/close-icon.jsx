'use strict';

const {React, ReactBase} = require('lib/tp/react-base');

class CloseIcon extends ReactBase {

    render() {

        return (
            <span className="glyphicon glyphicon-remove" onClick={this.props.handleClick}></span>
        )
    }
}

module.exports = CloseIcon;