'use strict';

const {React, ReactBase} = require('lib/tp/react-base');
const AltContainer = require('lib/tp/alt/AltContainer');

const DBEXActions = require('lib/da/dbex/actions/dbex');

const TableTree = require('lib/da/dbex/components/table-tree');
const TableTreeStore = require('lib/da/dbex/stores/table-tree');

const Board = require('lib/da/dbex/components/board');
const BoardStore = require('lib/da/dbex/stores/board');

const Header = require('lib/da/dbex/components/header');

class DBEX extends ReactBase {

    componentDidMount() {
        DBEXActions.fetchTableListing();
    }

    componentWillMount() {
        window.addEventListener('keyup', this.handleKeyup);
    }

    componentWillUnmount() {
        window.removeEventListener('keyup', this.handleKeyup);
    }

    handleKeyup(e) {
        if (e.shiftKey) { // shift
            if (e.keyCode == '76') { // L
                DBEXActions.clearBoard();
            } else if (e.keyCode == '78') { // N
                DBEXActions.spawnNewBoardItem();
            }
        }

    }

    handleCommitInput({boardItemId, serverName, tableName, columnName, columnValue}) {

        if (serverName) {
            serverName = this.findTableServer(tableName);
        }

        if (serverName) {
            DBEXActions.commitInput({boardItemId, serverName, tableName, columnName, columnValue});
        }

    }

    handleInsertDotSeparator({boardItemId, tableName}) {
        let serverName = this.findTableServer(tableName);

        if (serverName) {
            DBEXActions.insertDotSeparator({boardItemId, serverName, tableName});
        }
    }

    findTableServer(tableName) {

        // find out which server it is on
        let {tablesByServers} = this.props;

        // sometimes a table with a particular name can be on more than one server
        // this defines server priority when selecting which server to go with

        let serverPriority = new Set([...['main', 'split', 'msg', 'dev', 'sec'], ...Object.keys(tablesByServers)]);

        let tableServerName;

        serverPriority.forEach(server => {
            if (tableServerName) {
                return; // go with the first find
            }
            let serverTables = tablesByServers[server];
            if (serverTables.indexOf(tableName) > -1) {
                tableServerName = server;
            }
        });

        return tableServerName;
    }


    render() {

        let classNames = 'dbex';

        if (this.props.tableTreeExpanded) {
            classNames += ' table-tree-expanded';
        }

        return (
            <div className={classNames}>

                <Header />

                <AltContainer store={TableTreeStore}>
                    <TableTree />
                </AltContainer>

                <AltContainer store={BoardStore}>
                    <Board handleCommitInput={this.handleCommitInput}
                           handleInsertDotSeparator={this.handleInsertDotSeparator} />
                </AltContainer>

            </div>
        )
    }
}

module.exports = DBEX;