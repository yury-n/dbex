'use strict';

const {React, ReactBase} = require('lib/tp/react-base');
const AltContainer = require('lib/tp/alt/AltContainer');

const DBEXActions = require('lib/da/dbex/actions/dbex');

const TableAutosuggest = require('lib/da/dbex/components/table-autosuggest');
const TableAutosuggestStore = require('lib/da/dbex/stores/table-autosuggest');

const ColumnAutosuggest = require('lib/da/dbex/components/column-autosuggest');
const ColumnAutosuggestStore = require('lib/da/dbex/stores/column-autosuggest');

const BoardItem = require('lib/da/dbex/components/board-item');

class Board extends ReactBase {

    handleSelectSuggestedTable({suggestedTableWithServer, selectedBy}) {
        let boardItemId = this.props.items.find(item => item.active == true).id;
        DBEXActions.selectSuggestedTable({boardItemId, suggestedTableWithServer, selectedBy});
    }

    handleSelectSuggestedColumn(selectedColumn) {
        let boardItemId = this.props.items.find(item => item.active == true).id;
        DBEXActions.selectSuggestedColumn({boardItemId, selectedColumn});
    }

    handleDoubleClick(e) {
        if (['INPUT', 'TD', 'TH'].indexOf(e.target.tagName) > -1) {
            return;
        }
        DBEXActions.spawnNewBoardItem();
    }

    render() {
        var items = this.props.items;

        return (
            <div className="board" onDoubleClick={this.handleDoubleClick}>
                <div className="board-items">
                    {items.map(item => {
                        return <BoardItem key={item.id}
                                          {...item}
                                          handleCommitInput={this.props.handleCommitInput}
                                          handleInsertDotSeparator={this.props.handleInsertDotSeparator} />;
                    })}
                </div>
                <AltContainer store={TableAutosuggestStore}>
                    <TableAutosuggest handleSelectSuggestedTable={this.handleSelectSuggestedTable} />
                </AltContainer>
                <AltContainer store={ColumnAutosuggestStore}>
                    <ColumnAutosuggest handleSelectSuggestedColumn={this.handleSelectSuggestedColumn} />
                </AltContainer>
            </div>
        );
    }
}

module.exports = Board;