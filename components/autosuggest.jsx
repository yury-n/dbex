'use strict';

const {React, ReactBase} = require('lib/tp/react-base');

const DBEXActions = require('lib/da/dbex/actions/dbex');

class Autosuggest extends ReactBase {

    constructor(props) {
        super(props);

        this.propTypes = {
            active: React.PropTypes.bool,
            inputValue: React.PropTypes.string,
            componentPosition: React.PropTypes.object,
            suggestions: React.PropTypes.array,
            selectedIndex: React.PropTypes.number,

            handleBlur: React.PropTypes.func,
            handleUpdateSelectedIndex: React.PropTypes.func,
            handleSelectSuggestion: React.PropTypes.func
        };
    }

    componentWillMount() {
        window.addEventListener('keydown', this.handleKeydown);
        window.addEventListener('click', this.handleClickWindow);
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.handleKeydown);
        window.removeEventListener('click', this.handleClickWindow);
    }

    handleClickWindow() {
        // clicks on suggestions do not propagate events
        this.props.handleBlur();
    }

    handleMouseOverSuggestion(index) {
        this.props.handleUpdateSelectedIndex(index);
    }

    handleClickSuggestion(selectedIndex) {
        this.props.handleSelectSuggestion({selectedIndex, selectedBy: 'click'});
    }

    handleKeydown(e) {

        let {active, selectedIndex} = this.props;

        if (!active) {
            return;
        }

        if (e.keyCode == '38') { // up

            this.props.handleUpdateSelectedIndex(selectedIndex - 1);
            e.preventDefault();

        } else if (e.keyCode == '40') { // down

            this.props.handleUpdateSelectedIndex(selectedIndex + 1);
            e.preventDefault();

        } else if (e.keyCode == '13') { // enter

            this.props.handleSelectSuggestion({selectedIndex, selectedBy: 'enter'});
            e.preventDefault();

        } else if (e.keyCode == '9') { // tab

            this.props.handleSelectSuggestion({selectedIndex, selectedBy: 'tab'});
            e.preventDefault();

        } else if (e.keyCode == '27') { // esc

            this.props.handleBlur();
        }
    }

    render() {

        let {active, componentPosition, suggestions, className, selectedIndex} = this.props;

        let suggestionsListHTML = null;

        if (active) {

            let suggestionsHTML = [];

            suggestions.forEach((suggestionContent, index) => {

                let suggestionIsActive = (index == selectedIndex);

                suggestionsHTML.push(<Suggestion key={index}
                                                 index={index}
                                                 suggestionContent={suggestionContent}
                                                 active={suggestionIsActive}
                                                 handleMouseOver={this.handleMouseOverSuggestion}
                                                 handleClick={this.handleClickSuggestion} />);
            });
            suggestionsListHTML = <ul ref="ul" className="dropdown-menu">{suggestionsHTML}</ul>;
        }

        let classNames = 'autosuggest';

        if (className) {
            classNames += ' ' + className;
        }

        if (active) {
            classNames += ' open';
        }
        let divStyle = [];
        if (componentPosition) {
            divStyle.position = 'absolute';
            divStyle.left = componentPosition.left + 'px';
            divStyle.top = componentPosition.top + 'px';
        }

        return (
            <div className={classNames} ref="autosuggest" style={divStyle}>
                {suggestionsListHTML}
            </div>
        )
    }
}

class Suggestion extends ReactBase {

    handleMouseOver() {
        this.props.handleMouseOver(this.props.index);
    }

    handleClick(e) {
        this.props.handleClick(this.props.index);
        e.stopPropagation();
    }

    render() {

        let {active, suggestionContent} = this.props;

        let className = (active ? 'active' : '');

        return (
            <li className={className} onMouseOver={this.handleMouseOver} onClick={this.handleClick}>
                <a href="javascript:void(0);">
                    {suggestionContent}
                </a>
            </li>
        )
    }
}

module.exports = Autosuggest;