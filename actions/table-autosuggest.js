'use strict';

const alt = require('lib/da/dbex/alt-inst');

class TableAutosuggestActions {

    constructor() {
        this.generateActions(
            'updateSelectedIndex',
            'clearSuggestions'
        );
    }

}

module.exports = alt.createActions(TableAutosuggestActions);