'use strict';

const difi = require('lib/da/difi');
const alt = require('lib/da/dbex/alt-inst');

class DBEXActions {

    constructor() {

        this.generateActions(
            'expandTableTree',
            'collapseTableTree',
            // 'selectTableFromTableTree',

            'changeInput',
            // 'insertDotSeparator',
            // 'commitInput',

            // 'fetchTableListing',
            'fetchTableListingDone',
            // 'fetchTable',
            'fetchTableDone',
            // 'fetchTableColumns',
            'fetchTableColumnsDone',

            // 'selectSuggestedTable',
            'selectSuggestedColumn',

            'spawnNewBoardItem',
            'setActiveBoardItem',
            'removeBoardItem',

            'clearBoard'
        );
    }

    fetchTableListing() {

        this.dispatch();

        let tableListingFromCache = JSON.parse(localStorage.getItem('tableListing'));
        if (tableListingFromCache) {
            this.actions.fetchTableListingDone(tableListingFromCache);
        }

        difi.get('DBMeta', 'table_listing_smart', []).then(
            (response) => { /* success */
                this.actions.fetchTableListingDone(response);
                localStorage.setItem('tableListing', JSON.stringify(response));
            },
            (response) => { /* failure */ }
        );
        difi.send();

    }

    selectSuggestedTable(obj) {

        this.dispatch(obj);

        let {boardItemId, suggestedTableWithServer: {serverName, tableName}, selectedBy} = obj;

        if (selectedBy == 'enter') {
            this.actions.fetchTable({boardItemId, serverName, tableName});
        } else {
            this.actions.fetchTableColumns({boardItemId, serverName, tableName});
        }
    }

    fetchTable({boardItemId, serverName, tableName, columnName, columnValue}) {

        if (columnName && columnValue) {
            let query = columnName + '=' + columnValue;
            difi.get('DBMeta', 'table_query', [serverName, tableName, query]).then(
                (result) => { /* success */
                    this.actions.fetchTableDone({boardItemId, serverName, tableName, result});
                },
                (response) => { /* failure */ }
            );
        } else {
            difi.get('DBMeta', 'table_sample_smart', [serverName, tableName, 20, false]).then(
                (result) => { /* success */
                    this.actions.fetchTableDone({boardItemId, serverName, tableName, result});
                },
                (response) => { /* failure */ }
            );
        }
        difi.send();
    }

    fetchTableColumns({boardItemId, serverName, tableName}) {

        // for now it's implemented through the same endpoint as fetchTable
        // fetching rows as well
        // TODO: maybe make it a little bit faster by fetching only columns

        difi.get('DBMeta', 'table_sample_smart', [serverName, tableName, 20, false]).then(
            (result) => { /* success */
                let [columnNames] = result;
                this.actions.fetchTableColumnsDone({boardItemId, serverName, tableName, columnNames});
            },
            (response) => { /* failure */ }
        );
        difi.send();
    }

    commitInput(obj) {

        this.dispatch(obj);

        this.actions.fetchTable(obj);
    }

    insertDotSeparator(obj) {

        this.dispatch(obj);

        this.actions.fetchTableColumns(obj);
    }

    selectTableFromTableTree(obj) {

        this.dispatch(obj);

        let boardItemId = 0;
        let {serverName, tableName} = obj;

        this.actions.fetchTable({boardItemId, serverName, tableName});
    }


}

module.exports = alt.createActions(DBEXActions);