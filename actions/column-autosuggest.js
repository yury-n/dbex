'use strict';

const alt = require('lib/da/dbex/alt-inst');

class ColumnAutosuggestActions {

    constructor() {
        this.generateActions(
            'updateSelectedIndex',
            'clearSuggestions'
        );
    }

}

module.exports = alt.createActions(ColumnAutosuggestActions);