'use strict';

const { React, ReactDOM } = require('lib/tp/react-dom');

const App = require('lib/da/dbex/components/app');

ReactDOM.render(
    React.createElement(App),
    document.getElementById('js-react-app')
);